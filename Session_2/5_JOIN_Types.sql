-- Tworzenie nowej tabeli
CREATE TABLE tblTransaction (
	Amount				smallmoney NOT NULL,
	DateOfTransaction	smalldatetime NULL,
	EmployeeNumber		int NOT NULL
	)

INSERT INTO tblTransaction VALUES
(-964.05, '20150526', 658), 
(-105.23, '20150914', 987)

select * from tblTransaction

Truncate table tblTransaction
delete from tblTransaction

-- JOIN 

-- get transaction amount and employee data
select e.EmployeeNumber, e.EmployeeFirstName, e.EmployeeLastName 
	, sum(t.Amount) as 'Sum of amount'
from tblEmployee e
	JOIN tblTransaction t 
		ON e.EmployeeNumber = t.EmployeeNumber
	GROUP BY e.EmployeeNumber, e.EmployeeFirstName, e.EmployeeLastName
	ORDER BY EmployeeNumber
-- powyzszy join (INNER JOIN) nie zwraca danych pracownika (np 1046)
-- jesli nie wykonal zadnej transakcji
-- mimo ze jest on w tabeli pracownikow
select * from tblEmployee where EmployeeNumber = 1046
select * from tblTransaction where EmployeeNumber = 1046

-- Typy join�w (4 typy)
-- INNER JOIN - default (JOIN) - wartosc po ktorej jest join musi byc w obu tabelach
select e.EmployeeNumber, e.EmployeeFirstName
	, e.EmployeeLastName, sum(t.Amount) as 'Sum of amount'
from tblEmployee e
	INNER JOIN tblTransaction t ON e.EmployeeNumber = t.EmployeeNumber
	GROUP BY e.EmployeeNumber, e.EmployeeFirstName, e.EmployeeLastName
	ORDER BY EmployeeNumber

-- LEFT JOIN - wszystkie wartosci z lewej tabeli i jej odpowieniki lub null z prawej tabeli
select e.EmployeeNumber, e.EmployeeFirstName
	, e.EmployeeLastName, sum(t.Amount) as 'Sum of amount'
from tblEmployee e
	LEFT JOIN  tblTransaction t ON e.EmployeeNumber = t.EmployeeNumber
	GROUP BY e.EmployeeNumber, e.EmployeeFirstName, e.EmployeeLastName
	ORDER BY EmployeeNumber

-- RIGHT JOIN - wszystkie wartosci z prawej tabeli i jej odpowieniki lub null z lewej tabeli
select e.EmployeeNumber, e.EmployeeFirstName
	, e.EmployeeLastName, sum(t.Amount) as 'Sum of amount'
from tblEmployee e
	RIGHT JOIN  tblTransaction t ON e.EmployeeNumber = t.EmployeeNumber
	GROUP BY e.EmployeeNumber, e.EmployeeFirstName, e.EmployeeLastName
	ORDER BY EmployeeNumber

-- CROSS JOIN - kombinacja kazdego wiersza z prawej tabeli z kazdym wierszem z lewej tabeli

