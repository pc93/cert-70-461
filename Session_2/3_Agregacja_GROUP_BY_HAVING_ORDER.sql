-- Funkcje agregujace i sortujace

-- select people which born in each year
-- kolejnosc: select..from..where..group by
select year(DateOfBirth) as 'Year', count(*) as 'Num of Born'
	from tblEmployee
	group by year(DateOfBirth)

-- sprawdzenie
select * from tblEmployee where year(DateOfBirth) = 1990
-- kolejnosc wykonania
/*
from tblEmployee
where 1=1
group by year(DateOfBirth)
select year(DateOfBirth) as 'Year', count(*) as 'Num of Born'

Dlatego w group by nie mozna stosowac aliasow kolumn z linii 'select'
bo w group by nie sa one jeszcze znane
*/


select year(DateOfBirth) as 'Year', count(*) as 'Num of Born'
	from tblEmployee
	group by year(DateOfBirth)
	order by year(DateOfBirth) --default ascending

select year(DateOfBirth) as 'Year', count(*) as 'Num of Born'
	from tblEmployee
	group by year(DateOfBirth)
	order by year(DateOfBirth) desc -- descending, w Order by moze byc juz uzyty alias


-- how many employee start with each letter
select left(EmployeeLastName, 1) as Initial, count(*) as countOfInitial
	from tblEmployee
	group by left(EmployeeLastName, 1)
	order by countOfInitial desc

-- how many employee start with each letter - get top 5
select top(5) left(EmployeeLastName, 1) as Initial, count(*) as countOfInitial
	from tblEmployee
	group by left(EmployeeLastName, 1)
	order by countOfInitial desc


-- how many employee start with each letter - get all which aggegation is >= 50
select left(EmployeeLastName, 1) as Initial, count(*) as countOfInitial
	from tblEmployee
	group by left(EmployeeLastName, 1)
	having count(*) >= 50
	order by countOfInitial desc

-- dodanie 'where'
select left(EmployeeLastName, 1) as Initial, count(*) as countOfInitial
	from tblEmployee
	where DateOfBirth > '19800101'
	group by left(EmployeeLastName, 1)
	having count(*) >= 50
	order by countOfInitial desc

/* kolejnosc:
	from..where..group by..having..select..order by
wiec mozna uzywac aliasow
*/
