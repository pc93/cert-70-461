USE [70-461]
GO

--
Delete from tblEmployee

INSERT INTO tblEmployee (
	[EmployeeNumber],[EmployeeFirstName],[EmployeeMiddleName],[EmployeeLastName],
	[EmployeeGovernmentID],[DateOfBirth],[Department])
VALUES 
	(1, 'Dylan','A','Word','YO477721F', '19820314', 'Commercial'),
	(2, 'Dylan','A','Word','YO477721F', '19820314', 'Commercial')

-- Dodawanie moze byc tez przez Edit top 200 rows, wystarczy skopiowac dane z excela i wkleic do widoku Edit top 200 rows

select * from tblEmployee

-------------------------------------------------------
-- WHERE na typach tekstowych
-------------------------------------------------------


-- select lastname where it is 'Smith'
select * from tblEmployee where EmployeeLastName = 'Smith'

-- select lastname where it is earlier than 'Smith' (alphabetic order)
select * from tblEmployee where EmployeeLastName < 'Smith'
select * from tblEmployee where EmployeeLastName >= 'Smith'

-- % - 0 lub wiecej wystapien
-- _ - dokladnie 1 wystapienie

-- select lastname which start with 'S'
select * from tblEmployee where EmployeeLastName Like 'S%'

-- select lastname which end with 'S'
select * from tblEmployee where EmployeeLastName Like '%S'

-- select lastname which have 'S' in the middle, start or end with 'S'
select * from tblEmployee where EmployeeLastName Like '%S%'

-- select lastname which have 'S' at 2nd position
select * from tblEmployee where EmployeeLastName Like '_S%'

-- last name starts with R, S or T
select * from tblEmployee where EmployeeLastName Like '[r-t]%'

-- last name do NOT start with R, S or T
select * from tblEmployee where EmployeeLastName Like '[^rst]%'

-- find last name which start with % (in such case it has to be in [] brackets) 
select * from tblEmployee where EmployeeLastName Like '[%]%'

GO
-------------------------------------------------------
-- WHERE na typach numerycznych
-------------------------------------------------------
select * from tblEmployee where EmployeeNumber > 200
-- przeciwienstwo powyzszego zapytanie
select * from tblEmployee where EmployeeNumber <= 200
-- lub:
select * from tblEmployee where NOT EmployeeNumber > 200

-- negacja
select * from tblEmployee where EmployeeNumber != 200
select * from tblEmployee where EmployeeNumber <> 200

-- przedzial (AND, OR)
select * from tblEmployee where EmployeeNumber >= 200 and EmployeeNumber <= 209
select * from tblEmployee where NOT (EmployeeNumber >= 200 and EmployeeNumber <= 209)

select * from tblEmployee where EmployeeNumber < 200 or EmployeeNumber > 209

-- BETWEEN
select * from tblEmployee where EmployeeNumber between 200 and 209
select * from tblEmployee where EmployeeNumber not between 200 and 209

-- IN
select * from tblEmployee where EmployeeNumber in (200, 204, 209)


-------------------------------------------------------
-- WHERE na typach daty
-------------------------------------------------------
select * from tblEmployee where DateOfBirth between '19760101' and '19861231'

select * from tblEmployee where DateOfBirth >= '19760101' and DateOfBirth < '19861231'

select * from tblEmployee where year(DateOfBirth) between 1976 and 1986 -- DO NOT USE


