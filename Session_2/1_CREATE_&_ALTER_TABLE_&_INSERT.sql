-- 1. Tworzenie nowej tabeli

CREATE TABLE tblEmployee (
	EmployeeNumber			INT NOT NULL,
	EmployeeFirstName		VARCHAR(50) NOT NULL,
	EmployeeMiddleName		VARCHAR(50) NULL,
	EmployeeLastName		VARCHAR(50) NOT NULL,
	EmployeeGovernmentID	CHAR(10) NULL,
	DateOfBirth				DATE NOT NULL
	)

-- dodanie nowej kolumny
ALTER TABLE tblEmployee
ADD /* COLUMN - nie moze byc */ Department VARCHAR(10)

-- usuwanie nowej kolumny
ALTER TABLE tblEmployee
DROP COLUMN /* tym razem musi byc COLUMN */ Department3


-- modyfikacja kolumny, np rozszerzenie zakresu
ALTER TABLE tblEmployee
ALTER COLUMN Department VARCHAR(20)


-- Dodanie danych do tabeli
INSERT INTO tblEmployee 
VALUES (132, 'Dylan','A','Word','YO477721F', '19820314', 'Commercial')

INSERT INTO tblEmployee (
	[EmployeeNumber],[EmployeeFirstName],[EmployeeMiddleName],[EmployeeLastName],
	[EmployeeGovernmentID],[DateOfBirth],[Department])
VALUES (131, 'Dylan','A','Word','YO477721F', '19820314', 'Commercial')

SELECT * FROM tblEmployee
