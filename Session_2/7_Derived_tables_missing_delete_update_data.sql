-- find out employees which haven't got any transactions
select e.EmployeeNumber as ENum, e.EmployeeFirstName, e.EmployeeLastName,
		t.EmployeeNumber as TNum, sum(t.Amount) as 'Sum of amount'
from tblEmployee e
left join tblTransaction t on t.EmployeeNumber = e.EmployeeNumber 
where t.EmployeeNumber is null
group by e.EmployeeNumber, e.EmployeeFirstName, e.EmployeeLastName,
		t.EmployeeNumber
order by e.EmployeeNumber, e.EmployeeFirstName, e.EmployeeLastName,
		t.EmployeeNumber

-- as derived table
select * -- ENum, EmployeeFirstName, EmployeeLastName
from (
select e.EmployeeNumber as ENum, e.EmployeeFirstName, e.EmployeeLastName,
		t.EmployeeNumber as TNum, sum(t.Amount) as 'Sum of amount'
from tblEmployee e
left join tblTransaction t on t.EmployeeNumber = e.EmployeeNumber 
group by e.EmployeeNumber, e.EmployeeFirstName, e.EmployeeLastName,
		t.EmployeeNumber
) as temp
where TNum is null
order by ENum, EmployeeFirstName, EmployeeLastName, TNum -- order nie moze byc w derived table


-- zmiana left na right join
-- transakcje ktore zostaly stworzone przez nieistniejacych pracownikow
select * -- ENum, EmployeeFirstName, EmployeeLastName
from (
select e.EmployeeNumber as ENum, e.EmployeeFirstName, e.EmployeeLastName,
		t.EmployeeNumber as TNum, sum(t.Amount) as 'Sum of amount'
from tblEmployee e
right join tblTransaction t on t.EmployeeNumber = e.EmployeeNumber 
group by e.EmployeeNumber, e.EmployeeFirstName, e.EmployeeLastName,
		t.EmployeeNumber
) as temp
where ENum is null -- ENum zamiast TNum
order by ENum, EmployeeFirstName, EmployeeLastName, TNum

GO

-- usuwanie danych za pomoca derived tables
-- wszystkie transakcje ktore nie maja pasujacego pracwonika (id jest zbyt male)
select * -- ENum, EmployeeFirstName, EmployeeLastName
from (
select e.EmployeeNumber as ENum, e.EmployeeFirstName, e.EmployeeLastName,
		t.EmployeeNumber as TNum, t.Amount as 'Sum of amount'
from tblEmployee e
right join tblTransaction t on t.EmployeeNumber = e.EmployeeNumber 
group by e.EmployeeNumber, e.EmployeeFirstName, e.EmployeeLastName,
		t.EmployeeNumber, t.Amount
) as temp
where ENum is null -- ENum zamiast TNum
order by ENum, EmployeeFirstName, EmployeeLastName, TNum

select * from tblTransaction

-- usuwanie przez transakcje
begin transaction
	select count(*) from tblTransaction

	-- order: delete, from, where
	delete tblTransaction
	from tblEmployee e 
		right join tblTransaction t on t.EmployeeNumber = e.EmployeeNumber 
	where e.EmployeeNumber is null -- ENum zamiast TNum

	select count(*) from tblTransaction

rollback transaction -- rollback - przywroc poczatkowy stan, commit - zapisz aktualny stan


-- inny sposob usuwania danych
begin transaction
	select count(*) from tblTransaction
	
	-- order: delete, from, where
	delete tblTransaction
	from tblTransaction
	where EmployeeNumber IN (
		select TNum
		from (
		select e.EmployeeNumber as ENum, e.EmployeeFirstName, e.EmployeeLastName,
				t.EmployeeNumber as TNum, sum(t.Amount) as 'Sum of amount'
		from tblEmployee e
		right join tblTransaction t on t.EmployeeNumber = e.EmployeeNumber 
		group by e.EmployeeNumber, e.EmployeeFirstName, e.EmployeeLastName,
				t.EmployeeNumber
		) as temp
		where ENum is null -- ENum zamiast TNum
	)

	select count(*) from tblTransaction
rollback transaction

-- delete top(x)
begin transaction
	select count(*) 'Total trans' from tblTransaction
	
		select count(*) 'Before remove ''to delete'' count'
		from (
		select e.EmployeeNumber as ENum, e.EmployeeFirstName, e.EmployeeLastName,
				t.EmployeeNumber as TNum, sum(t.Amount) as 'Sum of amount'
		from tblEmployee e
		right join tblTransaction t on t.EmployeeNumber = e.EmployeeNumber 
		group by e.EmployeeNumber, e.EmployeeFirstName, e.EmployeeLastName,
				t.EmployeeNumber
		) as temp
		where ENum is null


	-- order: delete, from, where
	delete top(10) tblTransaction
	from tblTransaction
	where EmployeeNumber IN (
		select TNum
		from (
		select e.EmployeeNumber as ENum, e.EmployeeFirstName, e.EmployeeLastName,
				t.EmployeeNumber as TNum, sum(t.Amount) as 'Sum of amount'
		from tblEmployee e
		right join tblTransaction t on t.EmployeeNumber = e.EmployeeNumber 
		group by e.EmployeeNumber, e.EmployeeFirstName, e.EmployeeLastName,
				t.EmployeeNumber
		) as temp
		where ENum is null -- ENum zamiast TNum
	)

	select count(*) 'Total trans' from tblTransaction

	select count(*) 'After remove ''to delete'' count'
	from (
	select e.EmployeeNumber as ENum, e.EmployeeFirstName, e.EmployeeLastName,
			t.EmployeeNumber as TNum, sum(t.Amount) as 'Sum of amount'
	from tblEmployee e
	right join tblTransaction t on t.EmployeeNumber = e.EmployeeNumber 
	group by e.EmployeeNumber, e.EmployeeFirstName, e.EmployeeLastName,
			t.EmployeeNumber
	) as temp
	where ENum is null

rollback transaction
GO

-- aktualizacja danych
select * from tblEmployee where EmployeeNumber = 194
select * from tblTransaction where EmployeeNumber = 3
select * from tblTransaction where EmployeeNumber = 194

-- mozna usunac
--delete tblTransaction
--from tblTransaction
--where EmployeeNumber = 3

begin tran
	select * from tblTransaction where EmployeeNumber = 194

	-- order: update, set, from, where
	update tblTransaction
	set EmployeeNumber = 194
	from tblTransaction
	where EmployeeNumber = 3 -- could be 'between 3 and 10' or 'in (3, 4, 7)

	select * from tblTransaction where EmployeeNumber = 194

rollback tran

begin tran
	-- order: update, set, output, from, where
	update tblTransaction
	set EmployeeNumber = 194
	output inserted.*, deleted.*
	from tblTransaction
	where EmployeeNumber in (3, 4, 7)

rollback tran