-- create 3rd table
select Department, count(*) from tblEmployee
	group by Department

-- or
select temp.Department
FROM (
	select Department, count(*) as NumOfDepartment  -- derived table, tabela pochodna
	from tblEmployee
	group by Department
) as temp

-- or
select Department from tblEmployee group by Department

-- or
select DISTINCT Department from tblEmployee

-- Tworzenie nowej bazy z danych innej bazy
select DISTINCT Department, '' as DepartmentHead -- varchar(1) by default
into tblDepartment
from tblEmployee

drop table tblDepartment

select DISTINCT Department, convert(varchar(20), N'') as DepartmentHead 
into tblDepartment
from tblEmployee

alter table tblDepartment
alter column DepartmentHead varchar(30) null

select * from tblDepartment


GO

-- join 3rd table

select *
from tblDepartment d -- 1 department
join tblEmployee e on d.Department = e.Department -- 1 pracownik
join tblTransaction t on t.EmployeeNumber = e.EmployeeNumber -- 4 transakcje
order by e.EmployeeNumber -- wiec kilka wierszy dla jednego departamentu, pracownika, transakcji

select *
from tblDepartment d
left join tblEmployee e on d.Department = e.Department
left join tblTransaction t on t.EmployeeNumber = e.EmployeeNumber 
order by e.EmployeeNumber 

-- what is the total amount of transactions to each departments?
select d.Department, sum(t.Amount) as 'Sum of amount'
from tblDepartment d
left join tblEmployee e on d.Department = e.Department
left join tblTransaction t on t.EmployeeNumber = e.EmployeeNumber 
group by d.Department

insert into tblDepartment (Department, DepartmentHead)
	values ('Accounts', 'James')
select * from tblDepartment

select d.Department, d.DepartmentHead, sum(t.Amount) as 'Sum of amount'
from tblDepartment d
left join tblEmployee e on d.Department = e.Department
left join tblTransaction t on t.EmployeeNumber = e.EmployeeNumber 
group by d.Department, d.DepartmentHead
order by Department

select d.DepartmentHead, sum(t.Amount) as 'Sum of amount'
from tblDepartment d
left join tblEmployee e on d.Department = e.Department
left join tblTransaction t on t.EmployeeNumber = e.EmployeeNumber 
group by d.DepartmentHead
order by DepartmentHead


select e.EmployeeNumber as ENum, e.EmployeeFirstName, e.EmployeeLastName,
		t.EmployeeNumber as TNum, sum(t.Amount) as 'Sum of amount'
from tblEmployee e
left join tblTransaction t on t.EmployeeNumber = e.EmployeeNumber 
group by e.EmployeeNumber, e.EmployeeFirstName, e.EmployeeLastName,
		t.EmployeeNumber
order by e.EmployeeNumber, e.EmployeeFirstName, e.EmployeeLastName,
		t.EmployeeNumber
