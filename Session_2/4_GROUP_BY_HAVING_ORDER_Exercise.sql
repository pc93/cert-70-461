-- aktualizacja danych zgodnie z kursem
Update tblEmployee
Set EmployeeMiddleName = NULL
Where EmployeeMiddleName = ''

-- get each month in the date of birth and return the number of people
select datepart(month, DateOfBirth) as MonthNumber, count(*) as countOfPeople
	from tblEmployee
	group by month(DateOfBirth)
	order by month(DateOfBirth) desc

select datename(month, DateOfBirth) as MonthName, count(*) as countOfPeople
	from tblEmployee
	group by datename(month, DateOfBirth)
	order by datename(month, DateOfBirth) -- text order

select datename(month, DateOfBirth) as MonthName, count(*) as countOfPeople
	from tblEmployee
	group by datename(month, DateOfBirth), datepart(month, DateOfBirth)
	order by datepart(month, DateOfBirth) -- number order, sortowanie tylko po tym co jest w 'select' lub 'group by'

select datepart(month, DateOfBirth) as MonthNumber
	,  datepart(year, DateOfBirth) as YearNumber
	, count(*) as countOfPeople
	from tblEmployee
	group by year(DateOfBirth), month(DateOfBirth)
	order by year(DateOfBirth), month(DateOfBirth) desc


-- get employees which have middle name
/* 
Count(*) - count all the rows
Count(ColumnName) - count only these rowth which value of this specific columns is NOT null
*/
select datename(month, DateOfBirth) as MonthName
	,count(*) as countOfPeople
	,count(EmployeeMiddleName) as countOfPeopleWithMiddleName
	,count(*) - count(EmployeeMiddleName) as NoMiddleName
	from tblEmployee
	group by datename(month, DateOfBirth), datepart(month, DateOfBirth)
	order by datepart(month, DateOfBirth) -- number order, sortowanie tylko po tym co jest w 'select' lub 'group by'


-- what is the earliest and latest date of bitrh for each month
select datename(month, DateOfBirth) as MonthName
	,count(*) as countOfPeople
	,count(EmployeeMiddleName) as countOfPeopleWithMiddleName
	,count(*) - count(EmployeeMiddleName) as NoMiddleName
	,format(min(DateOfBirth), 'dd/MM/yyyy') as EarliestDateOfBirth
	,format(max(DateOfBirth), 'D', 'pl-PL') as LatestDateOfBirth
	from tblEmployee
	group by datename(month, DateOfBirth), datepart(month, DateOfBirth)
	order by datepart(month, DateOfBirth) -- number order, sortowanie tylko po tym co jest w 'select' lub 'group by'

