-- Implemnetacja bez kluczy
CREATE TABLE [dbo].[tblDepartment](
	[Department]		[varchar](20) NULL,
	[DepartmentHead]	[varchar](30) NULL
) 
-- LEFT JOIN TO:
CREATE TABLE [dbo].[tblEmployee](
	[EmployeeNumber]		[int] NOT NULL,
	[EmployeeFirstName]		[varchar](50) NOT NULL,
	[EmployeeMiddleName]	[varchar](50) NULL,
	[EmployeeLastName]		[varchar](50) NOT NULL,
	[EmployeeGovernmentID]	[char](10) NULL,
	[DateOfBirth]			[date] NOT NULL,
	[Department]			[varchar](20) NULL -- klucz do Department
)
--LEFT JOIN TO:
CREATE TABLE [dbo].[tblTransaction](
	[Amount]			[smallmoney] NOT NULL,
	[DateOfTransaction] [smalldatetime] NULL,
	[EmployeeNumber]	[int] NOT NULL -- klucz do Employee
) 

-- Constrains:
-- Unique
-- One column constraint
ALTER TABLE tblEmployee
ADD CONSTRAINT unqGovermentID UNIQUE ([EmployeeGovernmentID])

select * from tblEmployee

-- Many columns constraint
ALTER TABLE tblTransaction
ADD CONSTRAINT unqTransaction UNIQUE ([Amount],[DateOfTransaction],[EmployeeNumber])

select * from tblTransaction where EmployeeNumber = 131

--981,18	2015-04-21 00:00:00		131
--117,21	2014-05-31 00:00:00		131

delete from tblTransaction
where EmployeeNumber = 131

insert into tblTransaction values (1, '2015-04-21', 131)
insert into tblTransaction values (1, '2015-04-21', 131)

-- drop constraint
ALTER TABLE tblTransaction
DROP CONSTRAINT unqTransaction


-- create constraint with table
CREATE TABLE [dbo].[tblTransaction2](
		[Amount]			[smallmoney]	NOT NULL,
		[DateOfTransaction] [smalldatetime] NOT NULL,
		[EmployeeNumber]	[int]			NOT NULL,
	CONSTRAINT unqTransaction2 UNIQUE  ([Amount],[DateOfTransaction],[EmployeeNumber])
) 
drop table tblTransaction2


--select * from tblEmployee

--select e.EmployeeNumber as ENum
--	, t.EmployeeNumber as TNum
--	, sum(t.Amount) as 'Sum of amount'
--from tblTransaction t
--left join tblEmployee e 
--	on t.EmployeeNumber = e.EmployeeNumber 
--group by e.EmployeeNumber, t.EmployeeNumber
--order by e.EmployeeNumber

--begin tran
--	Update tblEmployee
--	set DateOfBirth = '2100-01-01'
--	where EmployeeNumber = 537
--	select * from tblEmployee order by DateOfBirth DESC
--rollback tran

--begin tran
--	Update tblEmployee
--	set EmployeeGovernmentID = 'aaa'
--	where EmployeeNumber between 530 and 539
--	select * from tblEmployee order by EmployeeGovernmentID ASC
--rollback tran