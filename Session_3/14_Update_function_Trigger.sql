Alter TRIGGER tr_tblTransaction
    ON dbo.tblTransaction
    AFTER DELETE, INSERT, UPDATE -- mozna wybrac kilka akcji dla triggera AFTER
    AS
    BEGIN
		--SET NOCOUNT ON -- wylacza output w  Messages

		--select @@NESTLEVEL 'Nest level - tr_tblTransaction'
		IF @@NESTLEVEL = 1 AND @@ROWCOUNT > 0
		BEGIN
			select *, 'Inserted' from inserted
			select *, 'Deleted'  from deleted
		END

		-- 1 sposob spradzenie czy dana koluma zostala zakualizowana
		IF UPDATE(Amount) -- wywolane tez przez insert bo zmienia sie z null na value
			SELECT 'Amount updated'
		
		-- 2 sposob sprawdzenia aktualizacji
		IF COLUMNS_UPDATED() & 1 = 1 -- COLUMNS_UPDATED - maksymalnie 8 kolumn
			SELECT 'Amount updated (2)'
		IF COLUMNS_UPDATED() & 2 = 2
			SELECT 'Date of Transaction updated (2)'
		IF COLUMNS_UPDATED() & 4 = 4
			SELECT 'Employee number updated (2)'
		IF COLUMNS_UPDATED() & 7 = 7
			SELECT 'All columns updated (2)'

		Declare @newAmount as smallmoney 
		Declare @oldAmount as smallmoney
		select @newAmount = Amount from inserted
		select @oldAmount = Amount from deleted

		IF @newAmount = @oldAmount
			Select 'Amount not changed'
	END
GO


-- INSERT
Begin tran
	insert into tblTransaction (Amount, DateOfTransaction, EmployeeNumber)
		VALUES (123, '2015-06-20', 123)
Rollback tran
GO

-- DELETE
Begin tran
	insert into tblTransaction (Amount, DateOfTransaction, EmployeeNumber)
		VALUES (123, '2015-06-20', 123)
	delete from tblTransaction 
		where Amount = 123 AND DateOfTransaction = '2015-06-20' AND EmployeeNumber = 123
	
	-- to nie wywola zadnego outputu w triggerze bo @@ROWCOUNT = 0
	delete from tblTransaction 
		where Amount = 123 AND DateOfTransaction = '2015-06-20' AND EmployeeNumber = 123
Rollback tran
GO

-- UPDATE
Begin tran
	insert into tblTransaction (Amount, DateOfTransaction, EmployeeNumber)
		VALUES (123, '2015-06-20', 123)
	UPDATE tblTransaction
		SET Amount = 321
		WHERE DateOfTransaction = '2015-06-20' AND EmployeeNumber = 123
Rollback tran
GO

Begin tran
	insert into tblTransaction (Amount, DateOfTransaction, EmployeeNumber)
		VALUES (123, '2015-06-20', 123)
	UPDATE tblTransaction
		SET DateOfTransaction = '2015-06-21'
		WHERE DateOfTransaction = '2015-06-20' AND EmployeeNumber = 123
Rollback tran
GO

Begin tran
	insert into tblTransaction (Amount, DateOfTransaction, EmployeeNumber)
		VALUES (123, '2015-06-20', 123)
	UPDATE tblTransaction
		-- update na taka sama wartosc, tez wywola te same procedury co przy zmianie na inna wartosc
		SET Amount = 123 
		WHERE DateOfTransaction = '2015-06-20' AND EmployeeNumber = 123
Rollback tran
GO

Begin tran
	insert into tblTransaction (Amount, DateOfTransaction, EmployeeNumber)
		VALUES (123, '2015-06-20', 123)
	UPDATE tblTransaction
		SET EmployeeNumber = 321
		WHERE DateOfTransaction = '2015-06-20' AND EmployeeNumber = 123
Rollback tran
GO
