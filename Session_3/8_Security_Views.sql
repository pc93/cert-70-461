select * from sys.syscomments 
select * from sys.views

 -- dbo - bazowanowa schema/owner, ktora jest wlaścicielem tabel i innych struktur bazodanowych
 -- jesli tabele z ktorych korzysta view sa zabezpieczone ale znajduja sie w tej samej schema co view to uprawnienia do tabel 
 -- nie sa sprawdzanie i view moze byc wykonane poprawnie
 -- nie mam dostepu do tabel bezposrednio, ale mam do nich dostep przez widok

 -- Jesli dbo.view korzysta z tabeli innej schema np dbo2.table3 to wszystko zalezy czy mamy uprawnienia do tabeli dbo2.table3

IF EXISTS(select * from INFORMATION_SCHEMA.VIEWS where  TABLE_NAME = 'ViewByDepartment' AND TABLE_SCHEMA = 'dbo')
 -- or EXISTS(select * from sys.views where name = 'ViewByDepartment')
	-- delete view
	DROP VIEW [dbo].[ViewByDepartment] 
GO

-- create view
CREATE VIEW [dbo].[ViewByDepartment] WITH ENCRYPTION AS -- dodane WITH ENCRYPTION!!!
	select d.Department, t.EmployeeNumber, t.DateOfTransaction, t.Amount  
	from tblDepartment d
	left join tblEmployee e on d.Department = e.Department
	left join tblTransaction t on t.EmployeeNumber = e.EmployeeNumber
	where t.EmployeeNumber between 120 and 139
	--order by d.Department, t.EmployeeNumber
GO