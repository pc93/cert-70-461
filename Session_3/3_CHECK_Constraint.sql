-- CHECK - a constrain which check if input value meet speficied requirements

-- Example 1
alter table tblTransaction
add constraint chkAmount check (Amount > -1000 and Amount < 1000)

insert into tblTransaction values (1001, '2015-10-10', 1)

alter table tblTransaction
drop constraint chkAmount

-- Example 2
alter table tblEmployee with nocheck -- with nockech - don't use it with existing rows
add constraint chkMidName check 
	(Replace(EmployeeMiddleName, '.', '') = EmployeeMiddleName or EmployeeMiddleName is null)
	-- B = B - valid = B = B. - invalid

begin tran -- invalid tran
	insert into tblEmployee
	values (2003, 'A', 'B.', 'C', 'D', '2014-01-01', 'Accounts')
	select * from tblEmployee where EmployeeNumber = 2003
rollback tran

begin tran -- valid tran
	insert into tblEmployee
	values (2003, 'A', 'B', 'C', 'D', '2014-01-01', 'Accounts')
	select * from tblEmployee where EmployeeNumber = 2003
rollback tran

begin tran -- valid tran
	insert into tblEmployee
	values (2003, 'A', null, 'C', 'D', '2014-01-01', 'Accounts')
	select * from tblEmployee where EmployeeNumber = 2003
rollback tran

alter table tblEmployee
drop constraint chkMidName


-- Example 3
alter table tblEmployee with nocheck
add constraint chkDateOfBirth check (DateOfBirth between '1900-01-01' and GETDATE())

begin tran -- valid tran
	insert into tblEmployee
	values (2003, 'A', 'B', 'C', 'D', '2015-01-01', 'Accounts')
	select * from tblEmployee where EmployeeNumber = 2003
rollback tran

begin tran -- invalid tran
	insert into tblEmployee
	values (2003, 'A', 'B', 'C', 'D', '2115-01-01', 'Accounts')
	select * from tblEmployee where EmployeeNumber = 2003
rollback tran

alter table tblEmployee
drop constraint chkDateOfBirth


-- Example 4
create table tblEmpl (
	EmplName varchar(6) null CONSTRAINT chkEmplName check (len(EmplName) between 3 and 6)
	)

begin tran -- invalid 
	insert into tblEmpl
	values ('aa')
	select * from tblEmpl
rollback tran

begin tran -- valid 
	insert into tblEmpl
	values ('aaa')
	select * from tblEmpl
rollback tran

begin tran -- valid 
	insert into tblEmpl
	values ('aaabbb')
	select * from tblEmpl
rollback tran

begin tran -- invalid 
	insert into tblEmpl
	values ('aaabbbc')
	select * from tblEmpl
rollback tran

begin tran -- invalid 
	insert into tblEmpl
	values (null)
	select * from tblEmpl
rollback tran

alter table tblEmpl
drop constraint chkEmplName

drop table tblEmpl

