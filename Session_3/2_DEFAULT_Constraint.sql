-- DEFAULT - set default value of column when row is created
-- especially useful when it is added to NOT NULL column
alter table tblTransaction
add DateOfEntry datetime

select * from tblTransaction

alter table tblTransaction
add constraint defDateOfEntry DEFAULT GETDATE() FOR DateofEntry -- FOR

delete from tblTransaction
where EmployeeNumber = 1

insert into tblTransaction(Amount, DateOfTransaction, EmployeeNumber) 
values (1, '2015-04-21', 1)
insert into tblTransaction(Amount, DateOfTransaction, EmployeeNumber, DateOfEntry)
 values (2, '2015-04-21', 1, '2015-04-23')

 

alter table tblTransaction
drop constraint defDateOfEntry

alter table tblTransaction
drop column DateOfEntry


-- create constraint with table
CREATE TABLE [dbo].[tblTransaction2](
		[Amount]			[smallmoney]	NOT NULL,
		[DateOfTransaction] [smalldatetime] NOT NULL,
		[EmployeeNumber]	[int]			NOT NULL,
		[DateOfEntry]		[datetime]		NULL CONSTRAINT defDateOfEntry2 DEFAULT GETDATE()
) 

insert into tblTransaction2(Amount, DateOfTransaction, EmployeeNumber) 
values (1, '2015-04-21', 1)
insert into tblTransaction2(Amount, DateOfTransaction, EmployeeNumber, DateOfEntry)
 values (2, '2015-04-21', 1, '2015-04-23')

select * from tblTransaction2

drop table tblTransaction2
