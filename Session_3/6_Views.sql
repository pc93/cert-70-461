-- views, purpose:
-- - restrict a user to specific rows in a table - allow an employee to see only the rows recording 
--      his work in a labor-tracking table
-- - restrict a user to specific columns - allow employees who do not work in payroll to the department, 
--      office and other details but don't allow to see salary or personal info
-- - join columns from multiple tables so that they look like a single table
-- - aggegate information instead of supplying/returning details


CREATE VIEW MyView AS

	select d.Department, t.EmployeeNumber, t.DateOfTransaction, t.Amount  
	from tblDepartment d
	left join tblEmployee e on e.Department = e.Department
	left join tblTransaction t on t.EmployeeNumber = e.EmployeeNumber
	where t.EmployeeNumber between 120 and 139
	order by d.Department, t.EmployeeNumber

GO -- to nie dzia�a, musi by� zdefiniowany TOP, albo usuniety ORDER BY

CREATE VIEW ViewByDepartment AS

	select d.Department, t.EmployeeNumber, t.DateOfTransaction, t.Amount  
	from tblDepartment d
	left join tblEmployee e on e.Department = e.Department
	left join tblTransaction t on t.EmployeeNumber = e.EmployeeNumber
	where t.EmployeeNumber between 120 and 139
	--order by d.Department, t.EmployeeNumber

GO
-- call a new view
select * from ViewByDepartment


 -- second view

CREATE VIEW ViewSummary AS

	select d.Department, t.EmployeeNumber, sum(t.Amount) TotalAmount
	from tblDepartment d
	left join tblEmployee e on e.Department = e.Department
	left join tblTransaction t on t.EmployeeNumber = e.EmployeeNumber
	where t.EmployeeNumber between 120 and 139
	group by d.Department, t.EmployeeNumber
	--order by d.Department, t.EmployeeNumber

GO
select * from ViewSummary