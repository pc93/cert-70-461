use [70-461]
go

-- Otworz Estimated execution plan (Ctrl + L)
-- seek - poszukiwanie - bazuje na indeksach
-- seek jest o wiele szybszy niz scan


 -- Pierwszy indeks stworzony w widoku musi byc Clustered
 -- potem mozna utworzyc dodatkowe indeksy non-clustered
 
 -- SELECT w widoku nie moze posiadac nastepujacych elementow:
 -- COUNT, OUTER joins (LEFT, RIGHT, FULL), DISTINCT, MIN, MAX, TOP, OFFSET, CTE, UNION

 -- funkcje wo�ane z widoku musza zawierac peln� referencje tzn schema.function
 select * from ViewByDepartment order by EmployeeNumber
 go

 IF EXISTS(select * from INFORMATION_SCHEMA.VIEWS where  TABLE_NAME = 'ViewByDepartment' AND TABLE_SCHEMA = 'dbo')
 -- or EXISTS(select * from sys.views where name = 'ViewByDepartment')
	-- delete view
	DROP VIEW [dbo].[ViewByDepartment] 
GO

-- create view
CREATE VIEW [dbo].[ViewByDepartment] with SCHEMABINDING AS -- changed line: added 'with SCHEMABINDING' - chroni podstawowe tabele przed zmianami
	select d.Department, t.EmployeeNumber, t.DateOfTransaction, t.Amount  
	from dbo.tblDepartment d -- changed line: added schema
	inner join dbo.tblEmployee e on d.Department = e.Department -- changed line: left -> inner
	inner join dbo.tblTransaction t on t.EmployeeNumber = e.EmployeeNumber -- changed line: left -> inner
	where t.EmployeeNumber between 120 and 139
	--order by d.Department, t.EmployeeNumber

GO

CREATE UNIQUE CLUSTERED INDEX idx_ViewByDepartment on dbo.ViewByDepartment (EmployeeNumber, Department, DateOfTransaction)
-- wartosci kolumn w indeksie musza byc unikalne dla kazdego wiersza!

begin tran
	delete from tblTransaction where EmployeeNumber = 131
	insert into tblTransaction VALUES	(1.00,	'2015-04-21 00:00:00', 131)

	drop table tblEmployee -- error (zabezpieczenie  with SCHEMABINDING): Cannot DROP TABLE 'tblEmployee' because it is being referenced by object 'ViewByDepartment'.
rollback tran