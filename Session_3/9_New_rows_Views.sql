IF EXISTS(select * from INFORMATION_SCHEMA.VIEWS where  TABLE_NAME = 'ViewByDepartment' AND TABLE_SCHEMA = 'dbo')
 -- or EXISTS(select * from sys.views where name = 'ViewByDepartment')
	-- delete view
	DROP VIEW [dbo].[ViewByDepartment] 
GO

-- create view
CREATE VIEW [dbo].[ViewByDepartment] AS
	select d.Department, t.EmployeeNumber, t.DateOfTransaction, t.Amount  
	from tblDepartment d
	left join tblEmployee e on d.Department = e.Department
	left join tblTransaction t on t.EmployeeNumber = e.EmployeeNumber
	where t.EmployeeNumber between 120 and 139
	--order by d.Department, t.EmployeeNumber
	WITH CHECK OPTION -- zapobiega dodawaniu danych do tabel przez widok,
					  -- pilnuje aby widocznosc danych byla taka sama jak po wykonaniu widoku
					  -- oraz nowe dane spelnialy warunek z WHERE
					  -- jesli nie jest to zwraca blad
					  -- http://zarez.net/?p=3002
GO


-- istnieje mozliwosc wstawiania danych do tabeli przez widok
begin tran
	select * from ViewByDepartment where EmployeeNumber = 132 order by Department, EmployeeNumber

	insert into ViewByDepartment(EmployeeNumber, DateOfTransaction, Amount)
		values (132, '2015-07-07', 93.93)

	select * from ViewByDepartment where EmployeeNumber = 132 order by Department, EmployeeNumber
	select * from tblTransaction where EmployeeNumber = 132
rollback tran


-- dodany Department, ale to juz nie zadziala bo widok moze sie odnosic tylko do jedej konkretnej tabeli podstawowej
--begin tran
--	select * from ViewByDepartment where EmployeeNumber = 132 order by Department, EmployeeNumber

--	insert into ViewByDepartment(Department ,EmployeeNumber, DateOfTransaction, Amount)
--		values ('Commercial', 132, '2015-07-07', 93.93)

--	select * from ViewByDepartment where EmployeeNumber = 132 order by Department, EmployeeNumber
--	select * from tblTransaction where EmployeeNumber = 132
--rollback tran


begin tran
	select * from ViewByDepartment order by EmployeeNumber, DateOfTransaction
	--select * from tblTransaction where EmployeeNumber in (132, 142)

	update ViewByDepartment
	set EmployeeNumber = 142
	where EmployeeNumber = 132

	select * from ViewByDepartment order by EmployeeNumber, DateOfTransaction
	--select * from tblTransaction where EmployeeNumber in (132, 142)
rollback tran