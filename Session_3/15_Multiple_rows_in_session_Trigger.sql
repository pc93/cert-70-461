Alter Trigger tr_ViewByDepartment
ON dbo.ViewByDepartment
INSTEAD OF DELETE -- w INSTEAD OF tylko jeden typ akcji moze by� stworzony dla danego triggera
AS
BEGIN
	SET NOCOUNT ON
	select * , 'To be deleted' FROM deleted

	select @@NESTLEVEL 'Nest level - tr_ViewByDepartment' -- Max nested level: 32
	
	--declare @EmployeeNumber as int
	--declare @DateOfTrans as smalldatetime
	--declare @Amount as smallmoney

	--select @EmployeeNumber = EmployeeNumber, 
	--	@DateOfTrans = DateOfTransaction, 
	--	@Amount = Amount
	--from deleted
	
	--delete from tblTransaction -- nested trigger: wywola trigger na tabeli Transaction (jesli taki istnieje)
	--	where EmployeeNumber = @EmployeeNumber
	--		AND DateOfTransaction = @DateOfTrans
	--		AND Amount = @Amount

	delete tblTransaction 
		from tblTransaction t
		INNER JOIN deleted d ON d.EmployeeNumber = t.EmployeeNumber 
			AND d.Amount = t.Amount 
			AND d.DateOfTransaction = t.DateOfTransaction

END

begin tran
	select *, 'Before delete' from ViewByDepartment where EmployeeNumber = 132
	delete from ViewByDepartment where EmployeeNumber = 132 -- AND Amount = -2.77
	select *, 'After delete' from ViewByDepartment where  EmployeeNumber = 132
rollback tran

