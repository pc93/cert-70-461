select * from INFORMATION_SCHEMA.VIEWS where  TABLE_NAME = 'ViewByDepartment' AND TABLE_SCHEMA = 'dbo'

IF EXISTS(select * from INFORMATION_SCHEMA.VIEWS where  TABLE_NAME = 'ViewByDepartment' AND TABLE_SCHEMA = 'dbo')
 -- or EXISTS(select * from sys.views where name = 'ViewByDepartment')
	-- delete view
	DROP VIEW [dbo].[ViewByDepartment] 
GO

-- create view
CREATE VIEW [dbo].[ViewByDepartment] AS
	select d.Department, t.EmployeeNumber, t.DateOfTransaction, t.Amount  
	from tblDepartment d
	left join tblEmployee e on d.Department = e.Department
	left join tblTransaction t on t.EmployeeNumber = e.EmployeeNumber
	where t.EmployeeNumber between 120 and 139
	--order by d.Department, t.EmployeeNumber
GO


-- From SQL Server 2016 SP1:
-- create view
CREATE OR ALTER VIEW [dbo].[ViewByDepartment] AS
	select d.Department, t.EmployeeNumber, t.DateOfTransaction, t.Amount  
	from tblDepartment d
	left join tblEmployee e on e.Department = e.Department
	left join tblTransaction t on t.EmployeeNumber = e.EmployeeNumber
	where t.EmployeeNumber between 120 and 139
	--order by d.Department, t.EmployeeNumber
GO


select * from ViewByDepartment
