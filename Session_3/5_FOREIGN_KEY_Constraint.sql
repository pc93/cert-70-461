-- FOREIGN KEY - it is a opposite to PRIMARY KEY
-- It is a reference to specific row in the table
-- We need primary key or unique constraint to use foreign key!

-- UNIQUE, CHECK, DEFAULT, PRIMARY KEY refers to the same table
-- ony FOREIGN KEY constraint connects two tables together

-- FK can be NULL!
-- Actions when PK or UNIQUE is changed and something relying on it (FK for example):
--	- no action - error (default behaviour)
--	- cascade - no errors
--  - set null - no errors
--  - set default - no errors if default exists

-- PK and FK data types have to ve identical!!!

use [70-461]
go

-- Syntax:
alter table tblTransaction WITH NOCHECK
	add constraint FK_tblTransaction_EmplNumber FOREIGN KEY (EmployeeNumber) 
	REFERENCES tblEmployee(EmployeeNumber)

alter table tblTransaction
drop constraint FK_tblTransaction_EmplNumber

 -- Example 0
Begin tran
 alter table tblTransaction ALTER COLUMN EmployeeNumber INT NULL
 alter table tblTransaction ADD CONSTRAINT DF_tblTransaction DEFAULT 124 FOR EmployeeNumber

alter table tblTransaction WITH NOCHECK
add constraint FK_tblTransaction FOREIGN KEY (EmployeeNumber) 
	REFERENCES tblEmployee(EmployeeNumber)

-- UPDATE tblEmployee SET EmployeeNumber = 9123 WHERE EmployeeNumber = 123
-- update returns error

select e.EmployeeNumber, t.* 
	from tblEmployee e
	RIGHT JOIN tblTransaction t
		on e.EmployeeNumber = t.EmployeeNumber
	where t.Amount in (-179.47, 786.22, -967.36, 957.03)

Rollback tran

-- Example 1 (Update
Begin tran
 alter table tblTransaction ALTER COLUMN EmployeeNumber INT NULL
 alter table tblTransaction ADD CONSTRAINT DF_tblTransaction DEFAULT 124 FOR EmployeeNumber

alter table tblTransaction WITH NOCHECK
add constraint FK_tblTransaction FOREIGN KEY (EmployeeNumber) 
	REFERENCES tblEmployee(EmployeeNumber)
	ON UPDATE NO ACTION

UPDATE tblEmployee SET EmployeeNumber = 9123 WHERE EmployeeNumber = 123
-- update returns error

select e.EmployeeNumber, t.* 
	from tblEmployee e
	RIGHT JOIN tblTransaction t
		on e.EmployeeNumber = t.EmployeeNumber
	where t.Amount in (-179.47, 786.22, -967.36, 957.03)

Rollback tran -- Result: ERROR


-- Example 2 (Update)
Begin tran
 alter table tblTransaction ALTER COLUMN EmployeeNumber INT NULL
 alter table tblTransaction ADD CONSTRAINT DF_tblTransaction DEFAULT 124 FOR EmployeeNumber

alter table tblTransaction WITH NOCHECK
add constraint FK_tblTransaction FOREIGN KEY (EmployeeNumber) 
	REFERENCES tblEmployee(EmployeeNumber)
	ON UPDATE CASCADE -- if there are any updates to the PK, they will be cascade like a waterfall to the FK key

UPDATE tblEmployee SET EmployeeNumber = 9123 WHERE EmployeeNumber = 123
-- DELETE FROM tblEmployee WHERE EmployeeNumber = 123

select e.EmployeeNumber, t.* 
	from tblEmployee e
	RIGHT JOIN tblTransaction t
		on e.EmployeeNumber = t.EmployeeNumber
	where t.Amount in (-179.47, 786.22, -967.36, 957.03)

Rollback tran

-- Example 3a (Update)
Begin tran
 alter table tblTransaction ALTER COLUMN EmployeeNumber INT NULL
 alter table tblTransaction ADD CONSTRAINT DF_tblTransaction DEFAULT 124 FOR EmployeeNumber

alter table tblTransaction WITH NOCHECK
add constraint FK_tblTransaction FOREIGN KEY (EmployeeNumber) 
	REFERENCES tblEmployee(EmployeeNumber)
	ON UPDATE SET NULL -- if there are any updates to the PK, they will be cascade like a waterfall to the FK key

UPDATE tblEmployee SET EmployeeNumber = 9123 WHERE EmployeeNumber = 123
-- DELETE FROM tblEmployee WHERE EmployeeNumber = 123

select e.EmployeeNumber, t.* 
	from tblEmployee e
	RIGHT JOIN tblTransaction t
		on e.EmployeeNumber = t.EmployeeNumber
	where t.Amount in (-179.47, 786.22, -967.36, 957.03)

Rollback tran


-- Example 3b (Update)
Begin tran
 alter table tblTransaction ALTER COLUMN EmployeeNumber INT NULL
 alter table tblTransaction ADD CONSTRAINT DF_tblTransaction DEFAULT 124 FOR EmployeeNumber

alter table tblTransaction WITH NOCHECK
add constraint FK_tblTransaction FOREIGN KEY (EmployeeNumber) 
	REFERENCES tblEmployee(EmployeeNumber)
	ON UPDATE SET DEFAULT -- if there are any updates to the PK, they will be cascade like a waterfall to the FK key

UPDATE tblEmployee SET EmployeeNumber = 9123 WHERE EmployeeNumber = 123
-- DELETE FROM tblEmployee WHERE EmployeeNumber = 123

select e.EmployeeNumber, t.* 
	from tblEmployee e
	RIGHT JOIN tblTransaction t
		on e.EmployeeNumber = t.EmployeeNumber
	where t.Amount in (-179.47, 786.22, -967.36, 957.03)

Rollback tran

-- Example 4 (delete)
Begin tran
 alter table tblTransaction ALTER COLUMN EmployeeNumber INT NULL
 alter table tblTransaction ADD CONSTRAINT DF_tblTransaction DEFAULT 124 FOR EmployeeNumber

alter table tblTransaction WITH NOCHECK
add constraint FK_tblTransaction FOREIGN KEY (EmployeeNumber) 
	REFERENCES tblEmployee(EmployeeNumber)
	--ON UPDATE CASCADE -- if there are any updates to the PK, they will be cascade like a waterfall to the FK key
	on DELETE NO ACTION

-- UPDATE tblEmployee SET EmployeeNumber = 9123 WHERE EmployeeNumber = 123
DELETE FROM tblEmployee WHERE EmployeeNumber = 123

select e.EmployeeNumber, t.* 
	from tblEmployee e
	RIGHT JOIN tblTransaction t
		on e.EmployeeNumber = t.EmployeeNumber
	where t.Amount in (-179.47, 786.22, -967.36, 957.03)

Rollback tran -- Result: ERROR


-- Example 5 (delete)
Begin tran
 alter table tblTransaction ALTER COLUMN EmployeeNumber INT NULL
 alter table tblTransaction ADD CONSTRAINT DF_tblTransaction DEFAULT 124 FOR EmployeeNumber

alter table tblTransaction WITH NOCHECK
add constraint FK_tblTransaction FOREIGN KEY (EmployeeNumber) 
	REFERENCES tblEmployee(EmployeeNumber)
	--ON UPDATE CASCADE -- if there are any updates to the PK, they will be cascade like a waterfall to the FK key
	on DELETE CASCADE
		
select * from tblTransaction where EmployeeNumber = 123

-- UPDATE tblEmployee SET EmployeeNumber = 9123 WHERE EmployeeNumber = 123
DELETE FROM tblEmployee WHERE EmployeeNumber = 123

select e.EmployeeNumber, t.* 
	from tblEmployee e
	RIGHT JOIN tblTransaction t
		on e.EmployeeNumber = t.EmployeeNumber
	where t.Amount in (-179.47, 786.22, -967.36, 957.03)

select * from tblTransaction where EmployeeNumber = 123

Rollback tran -- Result: No results

  -- Example 6a (delete)
Begin tran
 alter table tblTransaction ALTER COLUMN EmployeeNumber INT NULL
 alter table tblTransaction ADD CONSTRAINT DF_tblTransaction DEFAULT 124 FOR EmployeeNumber

alter table tblTransaction WITH NOCHECK
add constraint FK_tblTransaction FOREIGN KEY (EmployeeNumber) 
	REFERENCES tblEmployee(EmployeeNumber)
	--ON UPDATE CASCADE -- if there are any updates to the PK, they will be cascade like a waterfall to the FK key
	on DELETE SET NULL

-- UPDATE tblEmployee SET EmployeeNumber = 9123 WHERE EmployeeNumber = 123
DELETE FROM tblEmployee WHERE EmployeeNumber = 123

select e.EmployeeNumber, t.* 
	from tblEmployee e
	RIGHT JOIN tblTransaction t
		on e.EmployeeNumber = t.EmployeeNumber
	where t.Amount in (-179.47, 786.22, -967.36, 957.03)

Rollback tran -- Result: Null as EmployeeNumber (both tables)


  -- Example 6b (delete)
Begin tran
 alter table tblTransaction ALTER COLUMN EmployeeNumber INT NULL
 alter table tblTransaction ADD CONSTRAINT DF_tblTransaction DEFAULT 124 FOR EmployeeNumber

alter table tblTransaction WITH NOCHECK
add constraint FK_tblTransaction FOREIGN KEY (EmployeeNumber) 
	REFERENCES tblEmployee(EmployeeNumber)
	--ON UPDATE CASCADE -- if there are any updates to the PK, they will be cascade like a waterfall to the FK key
	on DELETE SET DEFAULT

-- UPDATE tblEmployee SET EmployeeNumber = 9123 WHERE EmployeeNumber = 123
DELETE FROM tblEmployee WHERE EmployeeNumber = 123

select e.EmployeeNumber, t.* 
	from tblEmployee e
	RIGHT JOIN tblTransaction t
		on e.EmployeeNumber = t.EmployeeNumber
	where t.Amount in (-179.47, 786.22, -967.36, 957.03)

Rollback tran -- Result: default value as EmployeeNumber (both tables)
