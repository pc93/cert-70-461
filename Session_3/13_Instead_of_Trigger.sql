CREATE Trigger tr_ViewByDepartment
ON dbo.ViewByDepartment
INSTEAD OF DELETE -- w INSTEAD OF tylko jeden typ akcji moze by� stworzony dla danego triggera
AS
BEGIN
	SET NOCOUNT ON

	select @@NESTLEVEL 'Nest level - tr_ViewByDepartment' -- Max nested level: 32
	
	declare @EmployeeNumber as int
	declare @DateOfTrans as smalldatetime
	declare @Amount as smallmoney

	select @EmployeeNumber = EmployeeNumber, 
		@DateOfTrans = DateOfTransaction, 
		@Amount = Amount
	from deleted
	
	delete from tblTransaction -- nested trigger: wywola trigger na tabeli Transaction (jesli taki istnieje)
		where EmployeeNumber = @EmployeeNumber
			AND DateOfTransaction = @DateOfTrans
			AND Amount = @Amount

	select * , 'ViewByDepartment' FROM deleted
END

begin tran
	select * from ViewByDepartment where Amount = -2.77 AND EmployeeNumber = 132
	delete from ViewByDepartment where Amount = -2.77 AND EmployeeNumber = 132
	select * from ViewByDepartment where Amount = -2.77 AND EmployeeNumber = 132
rollback tran
