-- PRIMARY KEY - similar to UNIQUE but no value could be null (even one)
-- main purpose is clustering/sorting
-- one per table
-- surrogate key if natural key can't be used
use [70-461]
go

 -- Example 1
alter table tblEmployee
add constraint PK_tblEmployee PRIMARY KEY (EmployeeNumber)

insert into tblEmployee (EmployeeNumber, EmployeeFirstName, EmployeeMiddleName,
	EmployeeLastName, EmployeeGovernmentID, DateOfBirth, Department)
	values (2004, 'AAA', 'BBB', 'CCC', 'AB12345FH', '2014-10-10', 'HR')

select * from tblEmployee order by EmployeeNumber desc

-- the same insert but GovID changes (unique constraint) 
-- result: duplication error
insert into tblEmployee (EmployeeNumber, EmployeeFirstName, EmployeeMiddleName,
	EmployeeLastName, EmployeeGovernmentID, DateOfBirth, Department)
	values (2004, 'AAA', 'BBB', 'CCC', 'AB12345FA', '2014-10-10', 'HR')

-- drop PK and try again
-- result: OK
alter table tblEmployee
drop constraint PK_tblEmployee

insert into tblEmployee (EmployeeNumber, EmployeeFirstName, EmployeeMiddleName,
	EmployeeLastName, EmployeeGovernmentID, DateOfBirth, Department)
	values (2004, 'AAA', 'BBB', 'CCC', 'AB12345FA', '2014-10-10', 'HR')

select * from tblEmployee order by EmployeeNumber desc

delete from tblEmployee
where EmployeeNumber = 2004


-- example 2
alter table tblEmployee
add constraint PK_tblEmployee PRIMARY KEY NONCLUSTERED (EmployeeNumber) -- clustered by default
-- add constraint PK_tblEmployee PRIMARY KEY NONCLUSTERED (EmployeeNumber)


-- example 3
-- identity(1, 1) - auto increment new rows, 
--		1st arg is the starting number, 2nd is increment value/number
--  identity could not be used with existing column, only new table or column
create table tblEmployee2 (
	EmployeeID int CONSTRAINT PK_tblEmployee2 PRIMARY KEY IDENTITY(1, 1),
	EmployeeName varchar(20)
	)
insert into tblEmployee2 (EmployeeName) values ('Pawel')
insert into tblEmployee2 (EmployeeName) values ('Tomek')
insert into tblEmployee2 (EmployeeName) values ('Janek')
select * from tblEmployee2

-- DELETE rows
delete from tblEmployee2
insert into tblEmployee2 (EmployeeName) values ('Pawel')
insert into tblEmployee2 (EmployeeName) values ('Tomek')

select * from tblEmployee2 -- IDs: 4, 5 (not 1, 2)

-- TRUNCATE rows
truncate table tblEmployee2
insert into tblEmployee2 (EmployeeName) values ('Pawel')
insert into tblEmployee2 (EmployeeName) values ('Tomek')
select * from tblEmployee2 -- IDs: 1, 2 (increment counter has been restarted!!)

-- not possible because of IDENTITY_INSERT is OFF
insert into tblEmployee2 (EmployeeID, EmployeeName) 
	values (3, 'Piotr'), (4, 'Michal')

SET IDENTITY_INSERT tblEmployee2 ON

insert into tblEmployee2 (EmployeeID, EmployeeName) 
	values (3, 'Piotr'), (4, 'Michal')

insert into tblEmployee2 (EmployeeID, EmployeeName) 
	values (34, 'Piotr'), (35, 'Michal')

select * from tblEmployee2

SET IDENTITY_INSERT tblEmployee2 OFF

select @@IDENTITY -- returns last identity used
select SCOPE_IDENTITY() -- returns what was last used in that scope (function/procedure etc)
select IDENT_CURRENT('dbo.tblEmployee2') -- return last identity used for a particular table

drop table tblEmployee2