-- DML - data manipulation language
-- DML events: INSERT, UPDATE, DELETE na tabeli lub widoku

-- DDL - data definition language
-- DDL events: CREATE, ALTER, DROP

-- 2 typy triggerow DML: AFTER (FOR) i INSTEAD OF

--INSERT INTO tblTransaction
--	VALUES (....) -- inserted and then call trigger
--	AFTER trigger
--		ROLLBACK Tran -- mozna przywrocic zmiany wprowadzone przez ATFER trigger

--INSERT INTO tblTransaction
--	VALUES (....) -- not inserted, INSTEAD of trigger is called
--	INSTEAD OF trigger 


-- wady triggerow:
 -- 1. Jesli robisz insert a w tle dodawane sa inne rzeczy, moze to wplynac na wydajnosc
 -- 2. Triggery sa 'schowane', przez co mozna zapomniec o tym ze istnieja 
 --    i podczas debugowania problemu moze byc ciezko dojsc skad wziely sie dodatkowe dane

 --  Max trigger nested level: 32

CREATE TRIGGER tr_tblTransaction
    ON dbo.tblTransaction
    AFTER DELETE, INSERT, UPDATE -- mozna wybrac kilka akcji dla triggera AFTER
    AS
    BEGIN
		SET NOCOUNT ON -- wylacza output w  Messages

		select @@NESTLEVEL 'Nest level - tr_tblTransaction'
		IF @@NESTLEVEL = 1
		BEGIN
			select * from inserted
			select * from deleted
		END
	END
GO

select * from sys.trigger_events
select * from sys.triggers

exec sp_configure 'nested triggers';

--exec sp_configure 'nested triggers', 0; -- wylaczenie zagniezdzonych triggerow
--RECONFIGURE
--GO

-- INSERT
Begin tran
	insert into tblTransaction (Amount, DateOfTransaction, EmployeeNumber)
		VALUES (123, '2015-06-20', 123)
Rollback tran
GO

-- DELETE
Begin tran
	insert into tblTransaction (Amount, DateOfTransaction, EmployeeNumber)
		VALUES (123, '2015-06-20', 123)
	delete from tblTransaction 
		where Amount = 123 AND DateOfTransaction = '2015-06-20' AND EmployeeNumber = 123
Rollback tran
GO

-- UPDATE
Begin tran
	insert into tblTransaction (Amount, DateOfTransaction, EmployeeNumber)
		VALUES (123, '2015-06-20', 123)
	UPDATE tblTransaction
		SET Amount = 321
		WHERE DateOfTransaction = '2015-06-20' AND EmployeeNumber = 123
Rollback tran
GO
