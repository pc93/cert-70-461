
IF EXISTS(select * from INFORMATION_SCHEMA.VIEWS where  TABLE_NAME = 'ViewByDepartment' AND TABLE_SCHEMA = 'dbo')
 -- or EXISTS(select * from sys.views where name = 'ViewByDepartment')
	-- delete view
	DROP VIEW [dbo].[ViewByDepartment] 
GO

-- create view
CREATE VIEW [dbo].[ViewByDepartment] AS
	select d.Department, t.EmployeeNumber, t.DateOfTransaction, t.Amount  
	from tblDepartment d
	left join tblEmployee e on d.Department = e.Department
	left join tblTransaction t on t.EmployeeNumber = e.EmployeeNumber
	where t.EmployeeNumber between 120 and 139
	--order by d.Department, t.EmployeeNumber

GO

select * from ViewByDepartment order by EmployeeNumber

-- nie mozna usunac wiersza z widoku bo odnosi sie do wielu tabeli, tak samo jak nie mozna bylo dodac wiersza
-- ktory odnosil sie do wielu tabeli (Trasactions, Departments)
delete from ViewByDepartment
	where Amount = 93.93 and EmployeeNumber = 132
GO

Create view ViewSimple AS
	Select * from tblTransaction
GO

Begin tran
	select * from ViewSimple where EmployeeNumber = 132

	delete from ViewSimple -- jesli widok zalezy od jednej tabeli, wtedy mozna usunac dane z tabeli przez widok
	where EmployeeNumber = 132

	select * from ViewSimple where EmployeeNumber = 132

rollback tran
