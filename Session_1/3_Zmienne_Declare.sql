-- Zmienne
-- Zmienne musza zaczynac sie znakiem @
-- AS - jest opcjonalne
-- Aby zmienic wartosc nalezy uzyc SET
DECLARE @myvar AS int = 0
DECLARE @myvar1 int = 1
DECLARE @myvar2 int
SET @myvar2 = 2

SELECT @myvar myvar

SET @myvar = 10
SELECT @myvar myvar

SET @myvar = @myvar + 2 -- 12
SET @myvar += 2 -- 14
SELECT @myvar myvar