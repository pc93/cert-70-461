-- Typy dat
-- Dokumentacja: Date and Data types

-- 6 roznych typow daty i czasu:

-- datetime - 8 bytes, dokladnosc:  3.333 milisekundy (3 miejsca po przecinku dla sekundy)
declare @date as datetime = '2019-10-29 18:57:56.232' -- tysieczna czesc sekundy moze sie konczyc tylko 0, 3 lub 7
declare @date2 as datetime = '2019-10-29 18:57:56.236'
declare @date3 as datetime = '2019-10-29 18:57:56.239'
declare @date4 as datetime = '20191029 18:57:56.232'
select @date 'datetime'
	, @date2 'datetime'
	, @date3 'datetime'
	, @date4 'datetime'


-- datetime2 - 6-8 bytes, dokladnosc:  100 ns, przechowywane wiecej liczb po przecinku dla sekundy
declare @date5 as datetime2(4) = '20191029 18:57:56.232' -- w nawiasie dokladnosc z jaka podawne sa czesci sekundy
select @date5 'datetime2'
-- dokladnosc: 
-- <= 2 miejsca po przecinku = 6 bajtow
-- 3 lub 4 miejsca = 7 bajtow
-- > 4 miejsca = 8 bajtow



-- date - 3 bytes, dokladnosc:  1 dzien
declare @date6 as date = '20191029 18:57:56.232'
select @date6 'date'

-- time - 3-5 bytes, dokladnosc:  100 ns
declare @time as time = '20191029 18:57:56.232'
select @time 'time'

-- smalldatetime - 4 bytes, dokladnosc:  1 minuta
declare @smalldatetime as smalldatetime = '20191029 18:57:05.032'
declare @smalldatetime2 as smalldatetime = '20191029 18:57:55.032'
select @smalldatetime 'smalldatetime', @smalldatetime2 'smalldatetime'


-- datetimeoffset - 8-10 bytes, dokladnosc: 100 ns
declare @datetimeoffset as datetimeoffset = '20191029 18:57:05.032'
select @datetimeoffset 'datetimeoffset'


-- Wbudowane funkcje
select 
	datefromparts(2019, 10, 29) as datefromparts
	,TIMEFROMPARTS(10, 11, 12, 123, 3) as timefromparts
	,DATETIME2FROMPARTS(2019, 10, 29, 10, 11, 12, 123, 3) datetime2fromparts

select @date 'pelna data'
	, year(@date) 'rok z daty'
	, month(@date) 'miesiac z daty'
	, day(@date) 'dzien z daty'


-- pobranie aktualne daty, 3 podstawowe funkcje
select CURRENT_TIMESTAMP as 'obecna data' -- typ: datetime
select getdate() as 'obecna data' -- typ: datetime
select SYSDATETIME() as 'obecna data' -- typ: datetime2

select @date 'data'
	,DATEADD(year, 1, @date) as 'dodanie 1 roku'
	,DATEADD(year, 2, @date) as 'dodanie 2 lat'
	,DATEADD(day, 1, @date) as 'dodanie 1 dnia'
	,DATEADD(hour, 2, @date) as 'dodanie 2 godzin'

select @date 'data'
	,datepart(MINUTE, @date) as 'datepart minute'
	,datename(WEEKDAY, @date) as 'datename week day'

select @date 'data1',
	DATEDIFF(day, @date, getdate()) 'roznica w dniach miedzy datami',
	DATEDIFF(minute, @date, getdate()) 'roznica w minutach miedzy datami',
	DATEDIFF(second, @date, getdate()) 'roznica w sekundach miedzy datami'