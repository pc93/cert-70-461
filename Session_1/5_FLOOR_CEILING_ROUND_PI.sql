-- funkcje matematyczne

DECLARE @myVar as numeric(7,2) = 3

select @myVar + 2 as dodawanie
	  ,@myVar - 2 as odejmowanie
	  ,@myVar * 2 as mnozenie
	  ,@myVar / 2 as dzielenie

-- POWER(Expression, power float)
select POWER(@myVar, 2) as potegowanie
	  ,SQUARE(@myVar) as potegowanie -- square - domyslnie potegowanie do kwadratu

select POWER(@myVar, 0.5) as pierwiastek
	  ,SQRT(@myVar) as pierwiastek

GO

DECLARE @positive as numeric (7,3) = 123.673
DECLARE @negative as numeric (7,3) = -123.673
-- FLOOR, CEILING, ROUND

select @positive as 'liczba dodatnia'
	 ,FLOOR (@positive) as sufit
	 ,CEILING (@positive) as podloga
	 ,@negative as 'liczba ujemna'
	 ,FLOOR (@negative) as sufit
	 ,CEILING (@negative) as podloga

Select @positive 'liczba'
	,ROUND (@positive, 1) as 'zaokraglenie do 1 miejsca'
	,ROUND (@positive, 2) as 'zaokraglenie do 2 miejsc'
	,ROUND (@positive, 3) as 'zaokraglenie do 3 miejsc'
	,ROUND (@positive, 0) as 'zaokraglenie do 0 miejsc'
	,ROUND (@positive, -1) as 'zaokraglenie do -1 miejsc'
	,ROUND (@positive, -2) as 'zaokraglenie do -2 miejsc'

declare @x as numeric(7,2) = 12.345
Select @x 'liczba'
	,ROUND(@x, 1) 'niestandardowe zachowanie'

-- PI i EXP
select PI() as myPI, EXP(1) as e

-- ABS
select ABS(@positive) as positive_abs
	,SIGN(@positive) as positive_sign
	,ABS(@negative) as negative_abs
	,SIGN(@negative) as negative_sign
	,ABS(0) as abs_0
	,SIGN(0) as sign_0

-- RAND
select rand(4565655)




