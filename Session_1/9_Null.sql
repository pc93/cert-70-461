-- Null

declare @int as int
-- SET @int = 4
select @int as 'wartosc liczby' 
	, @int + 2 + 2 as 'suma z nullem'
	, @int + 2 as 'roznica z nullem'
	, @int * 2 as 'iloczyn z nullem'
	, @int / 2 as 'iloraz z nullem'

--declare @compare as bit = @int <> 0


declare @string as varchar(10)
select @string as 'wartosc stringa'
	, left(@string, 2) as '2 skrajne lewe znaki'
	, len(@string) as 'dlugosc stringa'
	, DATALENGTH(@string) as 'datalength'


declare @decimal as decimal (5, 2)
select try_convert(decimal(5,2), 1000) as 'try_convert'
select try_cast(1000 as decimal(5,2)) as 'try_cast'
