-- Zmienne

-- Calkowito-liczbowe
-- bigint - 8 bajtow (-2^63; 2^63-1) 
-- int - 4 bajty (-2^31; 2^31-1), up to 2,000,000,000
-- smallint - 2 bajty (-2^15; 2^15-1) -32,767; 32,768
-- tinyint - 1 bajt (0; 255, bez ujemnych)

DECLARE @tiny tinyint = 255
-- SET @myvar -= 256
SET @tiny -= 0.5
SELECT @tiny 

DECLARE @small smallint = 255
SET @small -= 256

SELECT @small


-- Cwiczenie 1
DECLARE @var smallint
SET @var = 20000
SELECT @var

-- Cwiczenie 2
DECLARE @var2 int
SET @var2 = 200000
SELECT @var2

-- Cwiczenie 3
DECLARE @var3 int
SET @var3 = 200000
SELECT @var3


-- decimal i numeric
-- decimal to to samo co numeric
-- decimap (p, s), p - precision (domyslnie 18!), s - scale (domyslnie 0)
-- p = 1 - 9 = 5 bytes
-- p = 10-19 = 9 bytes
-- p = 20-28 = 13 bytes
-- p = 29-38 = 17 bytes
DECLARE @dec decimal(7,2) -- 7 cyfrowe liczby, gdzie 5 przed a 2 po przecinku
SET @dec = 11223.63
-- SET @dec = 112233.2 -- tylko 5 moze byc przed przecinkiem
SELECT @dec

-- drugi argument nie jest wymagany i wtedy zaokragla w gore! (zwykly int zaokragla w dol!!)
DECLARE @dec2 decimal(7) -- 7 cyfrowe liczby, gdzie 5 przed a 2 po przecinku
DECLARE @int2 int
SET @dec2 = 11223.63
SET @int2 = 11223.63
SELECT @dec2 as 'decimal'
SELECT @int2 as 'int'

DECLARE @dec3 decimal(5,5) = 0.123456
SELECT @dec3 as 'tylko ulamki p = s'


-- money i smallmoney (scale = 4, sta�e 4 miejsca po przecinku)
-- smallmoney - 4 bajty (-214,748.3648 ; 214,748,3647) 
-- money - 8 bajow  (-922,337,203,685,477.5808;  -922,337,203,685,477.5807)
DECLARE @smallm smallmoney = 123456.78917 -- zaokragla w gore po przecinku to co sie nie miesci
select @smallm as 'smallmoney' 


-- float i real - nie polecane do uzytku
-- wartosci sa przyblizane i nie wszystkie wartosci moga byc dokladnie przedstawione
-- float24 - p = 7 - 4 bajty
-- float53 - p = 15 - 8 bajtow
-- real to float24

DECLARE @float float(24) = 123456.7891
select @float as 'float' -- skrocilo do 7 cyft bo takie jest okraniczenie float24

SET @float = 123456.7891/100
select @float as 'float / 100'

SET @float = 123456.7891 * 100
select @float as 'float * 100'
