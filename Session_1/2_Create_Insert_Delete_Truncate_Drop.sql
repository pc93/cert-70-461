-- Tworzenie nowej tabeli
-- 1. Z GUI, 
-- 2. przez TSQL
CREATE TABLE tblSecond (
	myNumbers int
	)

-- Dodawanie wierszy do tabeli:
-- 1. Z GUI (PPM na tabele - Edit top 200 rows)
select * from tblFirst
-- 2. Z TSQL
INSERT INTO tblSecond			VALUES (1), (2), (3)
INSERT INTO [dbo].[tblSecond]	VALUES (4)

select * from tblSecond

-- Drukowanie/wyswietlanie wartosci:
-- * oznacza wszystkie kolumny jakie istniej� (ca�a tabela)
SELECT * FROM tblFirst
SELECT myNumbers FROM tblSecond

-- Usuwanie wierszy z tabeli
-- 1. Delete - usuwa dowolnie wybrane dane, zapisuje dane usuniecia w logach
-- Delete nie resetuje autoincrementu
SELECT * FROM tblSecond
DELETE FROM tblSecond -- usuwa wszystko z tabeli tylko zawartosc (nie tabele!)
SELECT * FROM tblSecond

-- 2. Truncate - usuwa wszystkie logi - szybszy od delete bo nie zapisuje operacji usuniecia w logach
-- Truncate - resetuje autoincrement, nowe rekordy numerowane od 1
SELECT * FROM tblSecond
TRUNCATE TABLE tblSecond -- usuwa tylko zawartosc tabeli ale nie tabele
SELECT * FROM tblSecond

-- 3. Drop
SELECT * FROM tblSecond
DROP TABLE tblSecond -- usuwa tabele

DELETE tblSecond
TRUNCATE tblSecond


-- Cwiczenia
Create table tblPrimeNumbers (
	intField int
	)
Insert into tblPrimeNumbers values (2), (3), (5), (7), (11)
select * from tblPrimeNumbers
Delete tblPrimeNumbers
truncate table tblPrimeNumbers
select * from tblPrimeNumbers
drop table tblPrimeNumbers