SELECT 1+1 AS Result
GO
SELECT 1*1 AS Result
GO
SELECT 1/0 AS Result -- GO konczy batch (partie)
Select 1/1 As Result
GO

SELECT 4+9 AS MyAnswer
SELECT 15-25 as Balance
SELECT 24*4+3 as MyResponse
SELECT 48/5 as 'Result without .'
SELECT 48/5.0 as 'Result with .'