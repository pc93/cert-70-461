﻿
-- Funkcje na typach tekstowych

-- varchar(max), nvarchar(max), max = 8000
declare @charASCII as varchar(10) = 'helloL'
declare @charUNICODE as nvarchar(10) = N'  helloڤ'

select @charASCII as charAscii
	, left(@charASCII, 2) as 'skrajne 2 znaki od lewej'
	, right(@charASCII, 2) as 'skrajne 2 znaki od prawej'
	, SUBSTRING(@charASCII, 2, 3) as '3 kolejne znaki od 2 znaku (wlacznie)' -- indeksowanie od 1
	, SUBSTRING(@charASCII, 4, 3) as '3 kolejne znaki od 4 znaku (wlacznie)'

select @charUNICODE as 'charUNICODE (2 spacje na poczatku)'
	, left(@charUNICODE, 2) as 'skrajne 2 znaki od lewej'
	, right(@charUNICODE, 2) as 'skrajne 2 znaki od prawej'
	, SUBSTRING(@charUNICODE, 2, 3) as '3 kolejne znaki od 2 znaku (wlacznie)' -- indeksowanie od 1
	, SUBSTRING(@charUNICODE, 4, 3) as '3 kolejne znaki od 4 znaku (wlacznie)'

select @charUNICODE as 'charUNICODE (2 spacje na poczatku)'
	, TRIM(@charUNICODE) as 'tekst po TRIM'
	, LTRIM(@charUNICODE) as 'tekst po LTRIM'
	, RTRIM(@charUNICODE) as 'tekst po RTRIM'

select @charASCII as charAscii
	, replace(@charASCII, 'l', 'L') as 'replace l with L'
	, upper(@charASCII) as 'upper - duze znaki'
	, lower(@charASCII) as 'lower - male znaki'
	, reverse(@charASCII) as 'reverse'
	, space(10) + 'hello' as '10 x space'
	, ASCII('a') as 'ascii - a'
	, UNICODE('a') as 'unicode - a'