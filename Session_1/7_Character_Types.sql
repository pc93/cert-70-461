﻿-- Typy tekstowe
-- A-Z a-z 0-9

-- char (ASCII) - 1 byte per character
declare @char as char(10)
set @char = ''
select @char as 'mychar'
	, len(@char) as 'length'
	, DATALENGTH(@char) as 'charLength'

-- varchar (ASCII) - 1 byte per character + 2 bytes
declare @varchar as varchar(10)
set @varchar = ''
select @varchar as 'myvarchar'
	, len(@varchar) as 'length'
	, DATALENGTH(@varchar) as 'charLength'

-- nchar (Unicode) - 2 bytes per character
declare @nchar as nchar(10)
set @nchar = N'ێ' -- implicit conversion with N''
select @nchar as 'mychar'
	, len(@nchar) as 'length'
	, DATALENGTH(@nchar) as 'charLength'

-- nvarchar (Unicode) - 2 bytes per character + 2 bytes
declare @nvarchar as nvarchar(10)
set @nvarchar = N'ێ'
select @nvarchar as 'myvarchar'
	, len(@nvarchar) as 'length'
	, DATALENGTH(@nvarchar) as 'charLength'
