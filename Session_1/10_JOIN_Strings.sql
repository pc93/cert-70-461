-- ��czenie stringow

declare @first as varchar(20)
declare @second as varchar(20)
declare @last as varchar(20)

set @first = 'Pawel'
set @last = 'Kowalski'

-- 1. null
select @first + ' ' + @second + ' ' + @last as 'Full name'

-- 1st option
select @first +  IIF(@second is null, '', ' ' + @second) + ' ' + @last as 'Full name - IIF'

-- 2nd option
select (@first 
	+ CASE 
		WHEN @second is null THEN '' 
		ELSE ' ' + @second 
		END
	+ ' ' + @last) as 'Full name - CASE'

-- 3rd option
select @first + coalesce(' ' + @second, '') + ' ' + @last as 'Full name - coalesce, bierze kolejne argumenty poki sa nullami'

-- 4th option
select CONCAT(@first, ' ' + @second, ' ', @last) as 'Full name - CONCAT'

set @second = 'Mateusz'
-- 1st option
select @first +  IIF(@second is null, '', ' ' + @second) + ' ' + @last as 'Full name - IIF'

-- 2nd option
select (@first 
	+ CASE 
		WHEN @second is null THEN '' 
		ELSE ' ' + @second 
		END
	+ ' ' + @last) as 'Full name - CASE'

-- 3rd option
select @first + coalesce(' ' + @second, '') + ' ' + @last as 'Full name - coalesce, bierze kolejne argumenty poki sa nullami'

-- 4th option
select CONCAT(@first, ' ' + @second, ' ', @last) as 'Full name - CONCAT'