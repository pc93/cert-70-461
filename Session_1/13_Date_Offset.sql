-- Strefy czasowe
-- size: 8-10 bytes
declare @datetimeoffset as datetimeoffset(3) = '20191029 18:57:05.032'
declare @datetimeoffset2 as datetimeoffset = '20191029 18:57:05.032 +05:30'
declare @datetimeoffset3 as datetimeoffset = '20191029 18:57:05.032 -01:00'
declare @datetimeoffset4 as datetimeoffset = '20191029 18:57:05.032 +13:59'
declare @datetimeoffset5 as datetimeoffset = '20191029 18:57:05.032 +14:00' -- max offset
select @datetimeoffset 'datetimeoffset'
	,@datetimeoffset2
	,@datetimeoffset3
	,@datetimeoffset4
	,@datetimeoffset5


declare @date as datetime = '2019-10-29 18:57:56.232'
select @date
	, TODATETIMEOFFSET(@date, '+05:30') as 'ToDateTimeOffset'
	, DATETIME2FROMPARTS(     2019, 4, 18, 1, 2, 3, 132,        3) 'Datetime2FromParts'
	, DATETIMEOFFSETFROMPARTS(2019, 4, 18, 1, 2, 3, 132, 5, 30, 3) 'datetimeOffsetFromParts'

select SYSDATETIMEOFFSET() 'sysdatetimeoffset GMT'
	, SYSUTCDATETIME() 'sysUtcDatetime UTC'

-- zmiana timezone'a - switchoffset
select @datetimeoffset2, SWITCHOFFSET(@datetimeoffset2, '-01:00') as 'switchoffset'