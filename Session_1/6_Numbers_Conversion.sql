--IMPLICIT 
declare @var as decimal(5,2) = 3
select @var/2

-- EXPLICIT
select CONVERT(decimal(5,2), 3) /2
select CAST(3 as decimal(5,2)) /2

select convert(decimal(5,2), 1000)

go 


select convert(int, 1.44) a
	  , convert(int, 1.6) b
	  , convert(int, 1.44) + convert(int, 1.6) 'convA + convB'
	  , convert(int, 1.44 +1.6) 'conv(a+b)'

select 3/2, 3.0/2

go

-- cwiczenia
-- 1.
select system_type_id
	, column_id
	, convert(decimal(5,2),system_type_id) / column_id as Calculation
from sys.all_columns

-- 2.
select system_type_id
	, column_id
	, floor(convert(decimal(5,2),system_type_id) / column_id) as Calculation
from sys.all_columns

-- 3.
select system_type_id
	, column_id
	, ceiling(convert(decimal(5,2),system_type_id) / column_id) as Calculation
from sys.all_columns

-- 4.
select system_type_id
	, column_id
	, round(convert(decimal(7,3),system_type_id) / column_id, 1) as Calculation
from sys.all_columns

-- 5.
select system_type_id 
	, column_id
	, TRY_CONVERT(tinyint, system_type_id * 2) as Calculation
from sys.all_columns