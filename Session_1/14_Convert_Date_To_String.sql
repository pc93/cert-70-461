
-- date to string
declare @date as datetime = '2019-10-29 18:57:56.232'
select 'The date and time is' + @date -- error
go

declare @date as datetime = '2019-10-29 18:57:56.232'
select 'The date and time is: ' + convert(nvarchar(20), @date) as 'Date and time'
	, 'The date and time is: ' + convert(nvarchar(20), @date, 101) as 'Date and time'
	, 'The date and time is: ' + convert(nvarchar(20), @date, 102) as 'Date and time'
	, 'The date and time is: ' + convert(nvarchar(20), @date, 103) as 'Date and time'
go

declare @date as datetime = '2019-10-29 18:57:56.232'
select 'The date and time is: ' + cast(@date as nvarchar(20)) as 'Date and time'
go

select format(cast('2019-10-29 18:57:56.232' as datetime), 'D') 'long date'
	, format(cast('2019-10-29 18:57:56.232' as datetime), 'd') 'short date (US date)'
	, format(cast('2019-10-29 18:57:56.232' as datetime), 'dd-MM-yyyy') 'European date'
	, format(cast('2019-10-29 18:57:56.232' as datetime), 'dd-/-MM-/-yyyy') 'My own format date'
	, format(cast('2019-10-29 18:57:56.232' as datetime), 'D', 'pl-PL')  'Polska data - pelna'
	, format(cast('2019-10-29 18:57:56.232' as datetime), 'd', 'pl-PL')  'Polska data - skrocona'


-- string to date
select try_convert(date, 'Thursday, 25 June 2015') as 'convert string to date' -- error
go

select parse('Thursday, 25 June 2015' as date) as 'parse string to date'
select try_parse('Monday, 25 June 2015' as date) as 'parse string to date' -- error, dni tygodnia musza sie zgadzac z rzeczywistoscia

go
select parse('Wtorek, 29 Października 2019' as date using 'pl-PL') as 'parse string to date, Polska'