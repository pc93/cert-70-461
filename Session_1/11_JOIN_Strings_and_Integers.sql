﻿-- Join string i inny typ
-- precedences - typy z mniejszym precendensem sa konwertowane do tych z wiekszym

select 'My number is: ' + '4567' as 'join of string'

-- varchar konwertowany do int, a nie odwrotnie, bo int ma wyzszy precedens
-- select 'My number is: ' + 4567 -- blad!
select 'My number is: ' + convert(varchar(20), 4567) as 'join of convert int'
select 'My number is: ' + cast(4567 as varchar(20)) as 'join of cast int'

select 'My salary is: $' + convert(varchar(20), 2345.6) as 'Salary' -- dziala ale nie tak jak chcemy
select 'My salary is: ' + format(2345.6, 'C') -- domyslnie w dolarach i 2 miejsca po przecinku
select 'My salary is: ' + format(2345.6, 'C3') -- 3 miejsca po przecinkuselect 'My salary is: ' + format(2345.6, 'C')
select 'My salary is: ' + format(2345.6, 'C', 'en-GB') -- ustawienie 'kultury'/waluty
select 'My salary is: ' + format(2345.6, 'C', 'fr-FR')
select 'My salary is: ' + format(2345.6, 'C', 'pl-PL')


-- cwiczenia:
select [name] from sys.all_columns

-- 1. Add the letter A to the end of each name.
select [name] + 'A' from sys.all_columns

-- 2. Add the letter Ⱥ to the end of each name
select [name] + N'Ⱥ' from sys.all_columns

-- 3. Remove the first character from name.
select [name], substring([name], 2, len([name]) - 1) from sys.all_columns -- -1 bo od znaku o nr 2 zostalo juz tylko len-1 znakow a nie cale len

-- 4. Remove the last original character from name.
select [name], substring([name], 1, len([name]) - 1) from sys.all_columns